# Frontend

This repository provides a web page to view the log record.

## Overview

Usage:

![usage][usage]

Snapshots:

![Snapshot of empty log record][snapshot-of-empty-log-record]

![Snapshot of test on css][snapshot-of-test-on-css]

Check [this][spec-doc] for the specification.

Check [DB schema][db-schema] and [backend source code][backend-dir] for more information.

## Installation

```zsh
# Precondition: change the current directory to this directory
npm install
```

## Run on your local device

```zsh
# Precondition: change the current directory to this directory
# To run on your local device instead of Docker to speed up the development
npm run start
```


[db-schema]: <../../docker/mariadb-docker-entrypoint-initdb.d/logging-service-schema-v0.0.0.sql>
[backend-dir]: <../frontend/>
[usage]: <docs/usage.png>
[snapshot-of-empty-log-record]: <docs/snapshot-of-empty-log-record.png>
[snapshot-of-test-on-css]: <docs/snapshot-of-test-on-css.png>

[spec-doc]: <https://infosmart.atlassian.net/l/c/25R1mfHE>
