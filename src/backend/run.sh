#!/bin/sh
pipenv install
PYTHONUNBUFFERED=1 PYTHONPATH=src exec pipenv run gunicorn --access-logfile=- --error-logfile=- --capture-output --log-level debug --worker-class=gevent --worker-connections=1000 --workers=$(($(nproc)*2+1)) --bind 0.0.0.0:16197 main:app
