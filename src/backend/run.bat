@echo off
pipenv install
SET FLASK_APP=src/main.py
SET FLASK_ENV=development
pipenv run python -m flask run --host=0.0.0.0 --port=16197

pause