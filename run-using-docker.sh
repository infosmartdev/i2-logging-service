#!/bin/bash

export MY_PROJECT_NAME=infosmart2_0-logging_service

docker-compose -p $MY_PROJECT_NAME down
docker-compose -p $MY_PROJECT_NAME build
docker-compose -p $MY_PROJECT_NAME up --detach

# this eventually runs run.sh in current folder
