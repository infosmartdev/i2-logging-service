import flask
import os
import sqlite3
import datetime
import hashlib
import json
import core
import pymysql
import core.config

def getDbConnection():
    return pymysql.connect(host=core.config.LOG_DATABASE_HOST,port=int(core.config.LOG_DATABASE_PORT),
        user=core.config.LOG_DATABASE_USER,
        password=core.config.LOG_DATABASE_PW,
        database=core.config.LOG_DATABASE_DB,
        cursorclass=pymysql.cursors.DictCursor)
