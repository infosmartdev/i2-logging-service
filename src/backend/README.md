# Backend

This repository provides RESTful APIs to fetch and insert log data.

## Overview

Check [this][spec-doc] for its specification.

Check [DB schema][db-schema] for more information.

## APIs tests (Postman)

Please check [postman directory][postman-dir] for the Postman collection and environment to test the RESTful APIs.

[db-schema]: <../../docker/mariadb-docker-entrypoint-initdb.d/logging-service-schema-v0.0.0.sql>
[postman-dir]: <postman>

[spec-doc]: <https://infosmart.atlassian.net/l/c/fhqfgRtM>
