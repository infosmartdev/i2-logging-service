import os

# Retrieve the configuration data from the environment which are set by docker-compose.yml.
# Default values are provided to develop without running docker-compose on this module.
LOG_DATABASE_USER = os.environ.get('LOG_DATABASE_USER', 'root')
LOG_DATABASE_PW = os.environ.get('LOG_DATABASE_PW', 'P@55itts')
LOG_DATABASE_HOST = os.environ.get('LOG_DATABASE_HOST', '127.0.0.1')
LOG_DATABASE_PORT = os.environ.get('LOG_DATABASE_PORT', '58416')
LOG_DATABASE_DB = os.environ.get('LOG_DATABASE_DB', 'log_db')
APP_ENV = os.environ.get('APP_ENV', 'DEV') # possible values: DEV / SIT / UAT / DEMO / PROD
