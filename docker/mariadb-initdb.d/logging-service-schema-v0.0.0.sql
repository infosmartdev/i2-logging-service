CREATE DATABASE `log_db`;
USE `log_db`;

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
    `id` int NOT NULL AUTO_INCREMENT,
    `time` datetime NOT NULL,
    `app` varchar(100) NOT NULL,
    `username` varchar(100) NOT NULL,
    `log_type` varchar(50) NOT NULL,
    `log_content` varchar(10000) NOT NULL DEFAULT '',

    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
