import React from 'react';
import ReactDOM from 'react-dom';
import Reactable from 'reactable';
import './basic-table-style.css';

var Table = Reactable.Table;

const logApiUrl = `${window.location.origin}/api/log`;
const fetchLogApiUrl = `${logApiUrl}/fetchLog`;

fetch(fetchLogApiUrl)
    // TODO: check status code of the response
    .then(response => response.json())
    .then(data => {
        // remap the keys to change the column names shown in the table view
        data = Object.entries(data.length ? data : [{}]).map(([key, value]) => {
            const remappedKeys = {};
            remappedKeys['ID'] = value['id'];
            remappedKeys['time'] = value['time'];
            remappedKeys['username'] = value['username'];
            remappedKeys['app'] = value['app'];
            remappedKeys['log type'] = value['log_type'];
            remappedKeys['log content'] = value['log_content'];
            return remappedKeys;
        });
        // render the log in a table view
        ReactDOM.render(
            <Table
            className="basic-table-style"
            id="table"
            data={data}
            filterable={Object.keys(data[0])}
            sortable={Object.keys(data[0])}
            // sort by ID instead of time to avoid a break in the reverse chronological order
            // caused by the "same" time values.
            // please note that the minimum time unit is second, which cannot represents a time difference of milliseconds.
            defaultSort={{column: 'ID', direction: 'desc'}} 
            itemsPerPage={8}
            />,
            document.getElementById('root')
        );
    });
