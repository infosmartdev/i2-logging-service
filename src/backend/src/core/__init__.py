import os
from flask import Flask
from flask_cors import CORS
import routes.log_routes

# Set the the name of the flask app, which defines the workspace of this app
app = Flask('Logging-Service-Backend')

# Read config data, e.g. the DB url
app.config.from_pyfile('src/core/config.py')

# Set the routes
app.register_blueprint(routes.log_routes.routes)

# This relaxes the same-origin policy, i.e. web pages from other origins may use this app's service
CORS(app, resources={r"/*": {"origins": "*"}})

# Set the logging config
app.logger.setLevel('DEBUG' if app.config['APP_ENV'] in ['DEV', 'SIT', 'UAT'] else 'ERROR')
