# Purpose: install the app dependency & set up a web server to host the ReactJS app.
# Usage: pass the desired port number to set up the web server; if no assigned port number, it taks 12345 as default
npm install --silent
PORT=${1:-12345} npm run start

# TODO: create a production build of the ReactJS app and at the same time, reserving the proxy server
