from core import app

class Logger:

    @staticmethod
    def logResponse(httpMethod, url, reqParam, response, classAndMethod):
        msg = "url="+url+", request="+str(reqParam)[0:1000]+", responseStatusCode="+str(response.status_code)+", responseText="+response.text.replace("\n", "")[0:1000]
        if response.status_code == 200:
            app.logger.info(classAndMethod+"(): "+msg)
        else:
            app.logger.error(classAndMethod+"(): "+msg)

    @staticmethod
    def error(msg, classAndMethod):
        app.logger.error(classAndMethod+"(): "+msg)

    @staticmethod
    def info(msg, classAndMethod):
        app.logger.info(classAndMethod+"(): "+msg)