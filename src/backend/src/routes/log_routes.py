import flask
from flask import request, Response, jsonify, make_response, current_app
import core
import pprint
from pymongo import MongoClient
from core import log_db
from bson import ObjectId
import json
from datetime import datetime
import os
import pathlib
#  import pystache
import weasyprint
from openpyxl.writer.excel import save_virtual_workbook

# TODO: pagination
# TODO: required to put a secret into the payload?

routes = flask.Blueprint('log', __name__)

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime): # massage datetime to iso format string
            return o.isoformat(' ',timespec='seconds')
        return json.JSONEncoder.default(self, o)

@routes.route(r'/fetchLog', methods=['GET'])
def fetchLog():
    'Fetch all log data and return in JSON format'
    'JSON example: '
    '[{"id": "0", "time": "2020-02-02 02:02:02", "app": "infoSMART mobile app", "username": "thomas.infoSMART", "log_type": "error", "log_content": "Invalid password."}]'
    current_app.logger.info('Entered fetchLog() in routes/log_routes.py')
    try: 
        with log_db.getDbConnection() as conn:
            with conn.cursor() as cursor:
                cursor.execute('''
                    SELECT
                        id, time, app, 
                        username, log_type, log_content
                        FROM log
                        ORDER BY time
                ''')
                rowsOfLog = cursor.fetchall()
                
                response = make_response(JSONEncoder().encode(rowsOfLog))
                response.headers['Content-Type'] = 'application/json'
                current_app.logger.info('Exited fetchLog() in routes/log_routes.py')
                return response
    except Exception as e:
        current_app.logger.error(f'Error: {e.args}')
        # return status code 500 & the error message in JSON
        return jsonify({'error': 'Internal Server Error', 'message': e.args}), 500



@routes.route(r'/insertLog', methods=['POST'])
def insertLog():
    'Read the log data from the payload and insert it into the database'
    'Expected payload: a json containing SINGLE log data - keys: app, username, log_type, log_content'
    'JSON exmpale: '
    '{"app": "infoSMART mobile app", "username": "thomas.infoSMART", "log_type": "error", "log_content": "Invalid password."}'
    'Expected result: the received log data will be inserted into the log_db database along with the current time as well as the auto assigned ID'
    current_app.logger.info('Entered insertLog() in routes/log_routes.py')
    try:
        with log_db.getDbConnection() as conn:
            with conn.cursor() as cursor:
                log = request.get_json()
                sql = ''' INSERT INTO log(time, app, username, log_type, log_content)
                          VALUES(%s, %s, %s, %s, %s)''' # parameterized query to protect from SQL injection attack
                cursor.execute(sql, (datetime.now().strftime("%Y/%m/%d %H:%M:%S"), 
                    log['app'], log['username'], log['log_type'], log['log_content']))
                conn.commit()
                current_app.logger.info('Exited insertLog() in routes/log_routes.py')
                # return status code 200
                return Response(status=200)
    except KeyError as e:
        current_app.logger.error(f'KeyError: error in reading the value of the "{e.args[0]}" key from the payload')
        # return status code 400 & the error message in JSON
        return jsonify({'error': 'Bad Request', 'message': f'Error in reading the value of the "{e.args[0]}" key from the payload'}), 400
    except Exception as e:
        current_app.logger.error(f'Error: {e.args}')
        # return status code 500 & the error message in JSON
        return jsonify({'error': 'Internal Server Error', 'message': e.args}), 500
