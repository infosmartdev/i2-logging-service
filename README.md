# Logging Service

In the client's mobile app, the log messages are only stored into their local devices. This project is initiated to establish a stand-alone web server & database to receive & save the logs from them.

## Overview

This project runs on docker & docker-compose. It is composed of 3 parts: [web server (Flask)][backend-dir], [relational DB (mariadb)][mariadb-dir] and [web page (ReactJS)][frontend-dir].

Check [this][high-lvl-doc] for more information.

## Run

```zsh
# Linux / MacOS
./run-using-docker.sh # this script runs the docker compose
```

## DB connection

```zsh
# Linux / MacOS
./.mysql.connect.mariadb.sh
```

[backend-dir]: <src/backend/>
[db-dir]: <docker/mariadb-docker-entrypoint-initdb.d/>
[frontend-dir]: <src/frontend/>
[mariadb-dir]: <docker/mariadb-initdb.d>

[high-lvl-doc]: <https://infosmart.atlassian.net/wiki/spaces/ITTS/pages/1723072513/v0.0.0+Logging+Service?atlOrigin=eyJpIjoiNzAzYzI5MDAxMDY0NDQ0ZWEzZmJiMGU4NTIxNDVkZDEiLCJwIjoiYyJ9>
