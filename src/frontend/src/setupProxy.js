/*
 * Please note that a lightweight web server is established to host the frontend app, i.e. the ReactJS app.
 * This script sets up a proxy server
 * and the proxy server forwards the API requests received by this lightweight web server to the backend server via the network set up by the docker compose.
 * The primary goal of this proxy server is to avoid the hard-code of the backend url in the frontend app
 * and its another benefit would be hiding/separating the backend from the client side.
 */
const  createProxyMiddleware  = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(
        '/api/log',
        createProxyMiddleware({
            // Forward the API requests to the backend via the network set up by the docker compose,
            // or using a hard-code url if this frontend app is not started by the docker compose
            target: process.env.LOG_API_URL || 'http://localhost:16197', // Supposed in the docker compose, LOG_API_URL environment variable is created to store the backend hostname
            logLevel: ['DEV', 'SIT', 'UAT'].includes(process.env.APP_ENV ?? '') ? 'debug' : 'error',
            changeOrigin: true,
            pathRewrite: {
                '^/api/log' : ''
            },
            prependPath: false,
            secure: false
        })
    );
};
